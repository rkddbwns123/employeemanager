package com.kyjg.employeemanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Getter
@Setter
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String employeeName;
    @Column(nullable = false,length = 20)
    private String employeePhone;
    @Column(nullable = false, length = 50)
    private String employeeAddress;
    @Column(nullable = false)
    private LocalDate birthDate;
    @Column
    private Integer age;
    @Column(nullable = false)
    private LocalDate enterCompanyDate;

    private LocalDate resignCompanyDate;
}
