package com.kyjg.employeemanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
public class EmployeeItem {
    private Long id;

    private String employeeName;

    private String employeePhone;

    private String employeeAddress;

    private LocalDate birthDate;

    private Integer age;

    private LocalDate enterCompanyDate;
}
