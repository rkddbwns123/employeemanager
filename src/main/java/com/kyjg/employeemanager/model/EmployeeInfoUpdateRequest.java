package com.kyjg.employeemanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class EmployeeInfoUpdateRequest {
    @NotNull
    @Length(min = 11, max = 20)
    private String employeePhone;
    @NotNull
    @Length(min = 2, max = 50)
    private String employeeAddress;
}
