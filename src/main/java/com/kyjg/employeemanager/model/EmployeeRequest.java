package com.kyjg.employeemanager.model;

import lombok.Getter;
import lombok.Setter;
import net.bytebuddy.asm.Advice;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
public class EmployeeRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String employeeName;
    @NotNull
    @Length(min = 11, max = 20)
    private String employeePhone;
    @NotNull
    @Length(min = 2, max = 50)
    private String employeeAddress;
    @NotNull
    private LocalDate birthDate;
}
