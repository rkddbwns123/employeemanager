package com.kyjg.employeemanager.controller;

import com.kyjg.employeemanager.model.EmployeeInfoUpdateRequest;
import com.kyjg.employeemanager.model.EmployeeItem;
import com.kyjg.employeemanager.model.EmployeeRequest;
import com.kyjg.employeemanager.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/employee")
public class EmployeeController {
    private final EmployeeService employeeService;
    @PostMapping("/data")
    public String setEmployee(@RequestBody @Valid EmployeeRequest request) {
        employeeService.setEmployee(request);

        return "OK";
    }
    @GetMapping("/all")
    public List<EmployeeItem> getEmployees() {
        List<EmployeeItem> result = employeeService.getEmployees();

        return result;
    }
    @PutMapping("/info/id/{id}")
    public String putEmployeeInfo(@PathVariable long id, @RequestBody @Valid EmployeeInfoUpdateRequest request) {
        employeeService.putEmployeeInfo(id, request);

        return "OK";
    }
    @PutMapping("/resign/id/{id}")
    public String putEmployeeResign(@PathVariable long id) {
        employeeService.putEmployeeResign(id);

        return "OK";
    }
    @DeleteMapping("/del-info/id/{id}")
    public String delEmployee(@PathVariable long id) {
        employeeService.delEmployee(id);

        return "OK";
    }
}
