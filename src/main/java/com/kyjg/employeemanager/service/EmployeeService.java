package com.kyjg.employeemanager.service;

import com.kyjg.employeemanager.entity.Employee;
import com.kyjg.employeemanager.model.EmployeeInfoUpdateRequest;
import com.kyjg.employeemanager.model.EmployeeItem;
import com.kyjg.employeemanager.model.EmployeeRequest;
import com.kyjg.employeemanager.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public void setEmployee(EmployeeRequest request) {
        Employee addData = new Employee();

        addData.setEmployeeName(request.getEmployeeName());
        addData.setEmployeePhone(request.getEmployeePhone());
        addData.setEmployeeAddress(request.getEmployeeAddress());
        addData.setBirthDate(request.getBirthDate());
        addData.setEnterCompanyDate(LocalDate.now());
        addData.setAge(LocalDate.now().getYear() + 1 - request.getBirthDate().getYear());

        employeeRepository.save(addData);
    }

    public List<EmployeeItem> getEmployees() {
        List<Employee> originList = employeeRepository.findAll();

        List<EmployeeItem> result = new LinkedList<>();

        for (Employee item : originList) {
            EmployeeItem addData = new EmployeeItem();

            Integer nowAge = LocalDate.now().getYear() + 1;
            Integer birthAge = item.getBirthDate().getYear();

            addData.setId(item.getId());
            addData.setEmployeeName(item.getEmployeeName());
            addData.setEmployeePhone(item.getEmployeePhone());
            addData.setEmployeeAddress(item.getEmployeeAddress());
            addData.setBirthDate(item.getBirthDate());
            addData.setAge(nowAge - birthAge);
            addData.setEnterCompanyDate(LocalDate.now());

            result.add(addData);
        }
        return result;
    }
    public void putEmployeeInfo(long id, EmployeeInfoUpdateRequest request) {
        Employee originData = employeeRepository.findById(id).orElseThrow();

        originData.setEmployeePhone(request.getEmployeePhone());
        originData.setEmployeeAddress(request.getEmployeeAddress());

        employeeRepository.save(originData);
    }
    public void putEmployeeResign(long id) {
        Employee originData = employeeRepository.findById(id).orElseThrow();

        originData.setResignCompanyDate(LocalDate.now());

        employeeRepository.save(originData);
    }

    public void delEmployee(long id) {
        employeeRepository.deleteById(id);
    }
}